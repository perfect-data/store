# Perfect Datastore

## Usage

```js
import Schema from '@perfect-data/schema';
import Store, { storeProvider } from '@perfect-data/store';


// register the store providers from schemas
Schema.use(storeProvider);


const movieSchema = new Schema({
   _id: String,
   title: { type: String, required: true },
   /*
   Here, releaseInformation is not a separate entity and belongs ONLY to this movie.
   As such, the sub-schema cannot have an _id field.
   */
   releaseInformation: SchemaType.ArrayOf(new Schema({
      category: { type: String, required: true, allowedValues: ['theater', 'dvd'] },
      date: { type: Date, required: true },
      description: String
   })),
   roles: SchemaType.ArrayOf(new Schema({
      /*
      Role name in movie script
      */
      role: { type: String, required: true },
      /*
      Actor's actual name
      */
      cast: { type: String, required: true }
   }))
}, {
   /*
   A schema name is required
   */
   name: 'movie',
   /*
   If not specified, the schema will NOT have a provider and will generate an error
   if any query specify it. The provider method takes only three arguments: the filter
   containing the search criteria, the options specifying any search options, and the
   context providing any method to retch the data from.
   */
   provider: async (filter, options, context) => context.db.movies.find(filter, options)
});

const actorSchema = new Schema({
   _id: String,
   firstName: { type: String, required: true },
   lastName: { type: String, required: true },
   birthdate: Date,
   /*
   Movie name
   */
   appearedIn: SchemaType.ArrayOf(String)
}, {
   name: 'actor',
   /*
   As long as the provider returns a collection, the retrieving function may be anything
   */
   provider: async (filter, options, context) => context
      .fetch(ACTORS_URL, { body: { filter, options } })
      .then(response => respnse.json())
});


/*
Search all movies with a title name containing "Star Wars"
*/
const query = `
movie(m.name like "%Star Wars%") m {
   title
   releasedDate alias first m.releaseInformation(ri.type = "theater") ri { 
      date
   }
   actors as all actor(m.name in a.appearedIn) a {
      * as any m.roles(r.cast = a.firstName " " a.lastName) r { 
         role
         cast
      }
   }
}
`;

const results = await Store.query(query);
/*
[{
   name: "Star Wars: A New Hope",
   releasedDate: "1977-05-25",
   actors: [
      { role: "Luke Skywalker", cast: "Mark Hamill" },
      { role: "Han Solo", cast: "Harrison Ford" },
      ...
   ]
}, ...]
*/
```
