# Perfect Query Syntax

The basic syntax is as follow:

```
$type ( $conditions ) $alias { $fields }
```


# $type

The type is any known named data type provider, or any literal collection.

For example: `movie() { title }` would return a collection of all movies mapped to a single field `title`.

For example: `m.roles() { cast }` would expect `m` to be an instance with an array field `roles`, and would map the `cast` field of each element of that array.


# $conditions

Conditions follow a SQL-like syntax and make use of define **alias** names.

For example: `movie(m.title ILIKE "%star wars%") m { title }` would return a collection of all movies that include the terms `"star wars"` (case insensitive) in their titles mapped to a single field `title`.

For example: `movie(m.releaseDate > "2000-01-01" AND m.title ILIKE "%star wars%") m { title, releaseDate }` would return a collection of movies released after `2000-01-01` and containing the terms `"star wars"` (case insensitive) in their titles mapped to two fields: `title` and `releaseDate`.

**Note:** the comma is only required if fields are put on the same line. The following query is also equivalent:

```
movie (m.releaseDate > "2000-01-01" AND m.title ILIKE "%star wars%") m {
   title
   releaseDate
}
```


# $alias

Used to identify an instance of a given type. Alias names are used to identify fields in searches, and are available recursively in sub-queries.

For example: `movie() m { title, actors as all actor(m.title in a.appearedIn) a { cast, role } }` would return a collection of movies mapped with the movie's `title` and a list of `actors` appearing in that movie, where `actor` is another type.


# $fields

Fields are an enumeration of properties that each data will make available from the query.

Fields may also be defined by sub-queries, such as:

```
  $field1 as [first|any|all] $type ( $condition ) $alias { $subfield1, ... }
  $field2 alias [first|any] $type ( $condition ) $alias { $subfield }
```

For example: `movie() m { title, mainActor as first actor(m.title in a.appearedIn) a { cast } }` would return a collection of movies mapped with the movie's `title` and another field mapped to the first `actor` found matching the condition.

**Note:** by default, `any` is assumed if omitted (i.e. `movie() m { title, randomActor as actor(m.title in a.appearedIn) a { cast } }`)

When `as` is specified:

* `first` return the first element of the collection (i.e. the field will be an object)
* `any` return a random element of the collection (i.e. the field will be an object)
* `all` return the entire collection as is (i.e. the field will be a collection)

When `alias` is specified, the sub-query *must* return a single field:

* `first` the field will be set to the first sub-field element of the collection
* `any` the field will be set to a random sub-field element of the collection