
class Store {
   constructor(options) {
      options = options || {};

      this.provider = options.provider;
      this.query = options.query;
      this.cache = options.cache;
   }


   async find(query, options) {
      if (this.provider) {
         const dataQuery = this.query ? this.query(query, options) : query;

         // TODO : fetch from cache

         const { items, total } = await this.provider.get(dataQuery, options);

         // TODO : set to cache

         return { items, total };
      }
   }

   async save(data) {
      throw new Error('Not implemented!');
   }
}



export default Store;