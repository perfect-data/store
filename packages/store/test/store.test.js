import Store from '../src/store';


describe('Testing store', () => {


   it('should retrive data with mock provider', async () => {
      const testData = [ { _id: 1, name: 'A' }, { _id: 2, name: 'B' } ];

      class MockProvider {
         async get(query, options) { 
            return {
               items: testData,
               total: testData.length
            };
         }
      };

      const store = new Store({ provider: new MockProvider() });

      const results = await store.find();

      expect(results.items).toEqual(testData);
   });


});